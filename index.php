<?php

require __DIR__ . '/vendor/autoload.php';

use Application\Stuff;
use Application\Controllers\Controller;
use Application\Models\Database;
use Application\boot\Data\SomeData;

$stuff = new Stuff();
var_dump($stuff);

$controller = new Controller();
var_dump($controller);

$database = new Database();
var_dump($database);

$someData = new SomeData();
var_dump($someData);